jQuery(function($){    
    'use strict';

    /**
     * Socket.IO code is collected in the IO namespace
     *
     */
    var IO = {

        /**
         * Connect the Socket.IO client
         * to Socket.IO server
         */
        init: function() {
            IO.socket = io.connect();
            IO.bindEvents();
        },

        bindEvents : function() {
            IO.socket.on('connected', IO.onConnected );
			IO.socket.on('error', IO.error);
        },

        onConnected : function() {
           console.log("sent message connected");
        },

   
        error : function(data) {
            alert(data.message);
        },
		
		smsServer: function(action){
			IO.socket.emit('controlAction', action);
		}

    };

    IO.init();
	
	/**
     * Setup code is collected in the Setup namespace
     *
     */
    var Setup = {

        init: function() {
            $('#controls > div').each(function(){
				var action = $(this).attr('id');
				if(action == 'undefined' || action == 'null' ) 
					return;
				$(this).click( function() { IO.smsServer(action) } );
			});
        }

    };

    Setup.init();

}($));