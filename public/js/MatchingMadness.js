"use strict";

var ID  ={};
var sprites = {};
var sounds = {};

Game.loadAssets = function () {
    var loadSprite = function (sprite) {
        return Game.loadSprite("../media/assets/MatchingMadness/" + sprite);
    };

    sprites.background = loadSprite("spr_background.jpg");
    sprites.single_jewel1 = loadSprite("spr_single_jewel1.png");
	sprites.single_jewel2 = loadSprite("spr_single_jewel2.png");
	sprites.single_jewel3 = loadSprite("spr_single_jewel3.png");
};

Game.initialize = function () {
	//define layer ids
    ID.layer_background = 1;
    ID.layer_objects = 20;

    //create game world
    Game.gameWorld = new MatchingMadnessGameWorld();
};