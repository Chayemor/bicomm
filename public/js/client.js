jQuery(function($){    
    'use strict';

    /**
     * Socket.IO code is collected in the IO namespace
     *
     */
    var IO = {

        /**
         * Connect the Socket.IO client
         * to Socket.IO server
         */
        init: function() {
            IO.socket = io.connect();
            IO.bindEvents();
        },

        bindEvents : function() {
            IO.socket.on('connected', IO.onConnected );
			IO.socket.on('controlAction', IO.controlAction ); 
			IO.socket.on('error', IO.error);
        },

        onConnected : function() {
           console.log("hi");
        },
		
		controlAction : function(action) { //User input
		   var action_func = GAME_LOGIC[action+""];
		   if( action_func != 'undefined' && action_func() )
				Game.handleInput();
        },
   
        error : function(data) {
            alert(data.message);
        }

    };

    
    IO.init();
	
	var GAME_LOGIC = {
		
		"up" : function() {
			var grid = Game.gameWorld.at(1);
			var dragRow = grid.getDragRow()-1 < 0 ? grid.rows-1 : grid.getDragRow()-1;
			grid.setDragRow(dragRow);
			return false;
		},
		"down" : function() {
			var grid = Game.gameWorld.at(1);
			grid.setDragRow(grid.getDragRow()+1);
			return false;
		},
		"right" : function() {
			console.log("going right");
			return true;
		},
		"left" : function() {
			console.log("going left");
			return true;
		},
		
	};

}($));