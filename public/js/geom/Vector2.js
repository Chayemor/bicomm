"use strict";

function Vector2(x, y) {
    this.x = typeof x !== 'undefined' ? x : 0;
    this.y = typeof y !== 'undefined' ? y : 0;
}

//Returns a 0,0 vector point 
Object.defineProperty(Vector2, "zero",
    {
        get: function () { return new Vector2();   }
    }
);

// isZero
Object.defineProperty(Vector2.prototype, "isZero",
    {
        get: function () { return this.x === 0 && this.y === 0;  }
    }
);

// length
Object.defineProperty(Vector2.prototype, "length",
{
	get: function () {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}
});

//copy instance
Vector2.prototype.copy = function () {
    return new Vector2(this.x, this.y);
};

Vector2.prototype.toString = function () {
    return "(" + this.x + ", " + this.y + ")";
};

Vector2.prototype.normalize = function () { //Make vector have length = 1
    var length = this.length;
    if (length === 0) //vector is (0,0)
        return;
    this.divideBy(length);
};

/* Following are math operations. 
* If function has "To,From, With" appended to name, the math operation is carried out
* on the instance of Vector2 that invoked the method. 
* Otherwise, the instance that ivoked the method is copied, and the math operation is applied 
* to that copy, and returned as a result of the function.
*/

Vector2.prototype.addTo = function (v) {
    if (v.constructor === Vector2) { //make sure parameter is of the same type
        this.x += v.x;
        this.y += v.y;
    }
    else if (v.constructor === Number) {
        this.x += v;
        this.y += v;
    }
    return this; //Allow chain method calls
};

Vector2.prototype.add = function (v) {
    var result = this.copy();
    return result.addTo(v);
};

Vector2.prototype.subtractFrom = function (v) {
    if (v.constructor === Vector2) {
        this.x -= v.x;
        this.y -= v.y;
    }
    else if (v.constructor === Number) {
        this.x -= v;
        this.y -= v;
    }
    return this;
};

Vector2.prototype.subtract = function (v) {
    var result = this.copy();
    return result.subtractFrom(v);
};

Vector2.prototype.divideBy = function (v) {
    if (v.constructor === Vector2) {
        this.x /= v.x;
        this.y /= v.y;
    }
    else if (v.constructor === Number) {
        this.x /= v;
        this.y /= v;
    }
    return this;
};

Vector2.prototype.divide = function (v) {
    var result = this.copy();
    return result.divideBy(v);
};

Vector2.prototype.multiplyWith = function (v) {
    if (v.constructor === Vector2) {
        this.x *= v.x;
        this.y *= v.y;
    }
    else if (v.constructor === Number) {
        this.x *= v;
        this.y *= v;
    }
    return this;
};

Vector2.prototype.multiply = function (v) {
    var result = this.copy();
    return result.multiplyWith(v);
};

Vector2.prototype.equals = function (obj) {
    return this.x === obj.x && this.y === obj.y;
};