"use strict";

/*
* x,y = Top left Corner of Rectangle
*/
function Rectangle(x,y, width, height){
	this.x = typeof x !== 'undefined' ? x : 0;
	this.y = typeof y !== 'undefined' ? y : 0;
	this.width = typeof width !== 'undefined' ? width : 1;
	this.height = typeof height !== 'undefined' ? height : 1;
}

//Position
Object.defineProperty(Rectangle.prototype, "position",
    {
        get: function () { return new Vector2(this.x, this.y);    }
    }
);
	
//Size
Object.defineProperty(Rectangle.prototype, "size",
    {
        get: function () { return new Vector2(this.width, this.height);     }
    }
);

//Left Side
Object.defineProperty(Rectangle.prototype, "left", 
	{
		get: function() { return this.x ; }
	}
);

//Right Side
Object.defineProperty(Rectangle.prototype, "right", 
	{
		get: function() { return this.x + this.width; }
	}
);

//Top Side
Object.defineProperty(Rectangle.prototype, "top", 
	{
		get: function() { return this.y; }
	}
);

//Bottom Side
Object.defineProperty(Rectangle.prototype, "bottom", 
	{
		get: function() { return this.y + this.height; }
	}
);

//Center Side
Object.defineProperty(Rectangle.prototype, "center", 
	{
		get: function() { return this.position.addTo( this.size.divideBy(2) ); }
	}
);

//Rectangle contains vector point
Rectangle.prototype.contains = function (vector_point) {
    vector_point = typeof vector_point !== 'undefined' ? vector_point : new Vector2();
    return (vector_point.x >= this.left && vector_point.x <= this.right &&
        vector_point.y >= this.top && vector_point.y <= this.bottom);
};

//Rectangle intersects with another rectangle
Rectangle.prototype.intersects = function (rect) {
    return (this.left <= rect.right && this.right >= rect.left &&
        this.top <= rect.bottom && this.bottom >= rect.top);
};

//Draw
Rectangle.prototype.draw = function () {
    Canvas2D.drawRectangle(this.x, this.y, this.width, this.height);
};