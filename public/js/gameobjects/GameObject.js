"use strict";

// delta is elapsed time

function GameObject(layer) {
	this.layer = layer !== 'undefined' ? layer : 0; //Low number layers will be drawn first
	this.parent = null;
	this.position = Vector2.zero;
	this.velocity = Vector2.zero;
	this._visible = true;
}

GameObject.prototype.handleInput = function(delta){}
GameObject.prototype.draw = function(){}

GameObject.prototype.update = function (delta) {
	this.position.addTo(this.velocity.multiply(delta));
}

GameObject.prototype.reset = function () {
    this._visible = true;
};

Object.defineProperty(GameObject.prototype, "visible",
{
	get: function () {
		return  this.parent === null ?
				this._visible :
				this._visible && this.parent.visible;	
	},

	set: function (value) { this._visible = value; 	}
});

Object.defineProperty(GameObject.prototype, "root",
{
	get: function () {
		return  this.parent === null ?
				this :
				this.parent.root;
	}
});

Object.defineProperty( GameObject.prototype , "worldPosition" ,
	{
		get: function() {
			return  this.parent !== null ?
					this.parent.worldPosition.addTo(this.position) :
					this.position.copy();
		}
	}
);