"use strict";

//Inherits from GameObject

function GameObjectList(layer) {
	GameObject.call(this, layer);
	this._gameObjects = [];
}

GameObjectList.prototype = Object.create(GameObject.prototype);

//GameObjects closes to the user should handle Input first
GameObjectList.prototype.handleInput = function (delta) {
    for (var i = this._gameObjects.length - 1; i >= 0; --i)
        this._gameObjects[i].handleInput(delta);
};

GameObjectList.prototype.update = function(delta) {
	for(var i=0, limit = this._gameObjects.length; i < limit; ++i)
		this._gameObjects[i].update(delta);
};

GameObjectList.prototype.draw = function() {
	if(!this.visible) 
		return;
	
	for(var i=0, limit = this._gameObjects.length; i < limit; ++i){ 
		if( this._gameObjects[i].visible )
			this._gameObjects[i].draw();
	}
};

GameObjectList.prototype.add = function(gameObject) {
	this._gameObjects.push(gameObject);
	gameObject.parent = this;
	// If sort func returns postivie number then a > b
	this._gameObjects.sort( function(a,b){
		return a.layer - b.layer;	
	});
};

GameObjectList.prototype.remove = function (gameobject) {
    for (var i = 0, limit = this._gameObjects.length; i < limit; ++i) {
        if (gameobject !== this._gameObjects[i])
            continue;
        this._gameObjects.splice(i, 1);
        gameobject.parent = null;
        return;
    }
};

GameObjectList.prototype.at = function (index) {
    if (index < 0 || index >= this._gameObjects.length)
        return null;
    return this._gameObjects[index];
};

GameObjectList.prototype.clear = function () {
    for (var i = 0, limit = this._gameObjects.length; i < limit; ++i)
        this._gameObjects[i].parent = null;
    this._gameObjects = [];
};

GameObjectList.prototype.reset = function () {
    for (var i = 0, limit = this._gameObjects.length; i < limit; ++i)
        this._gameObjects[i].reset();
};

Object.defineProperty(GameObjectList.prototype, "length", 
{
    get: function () { return this._gameObjects.length;   }
});
