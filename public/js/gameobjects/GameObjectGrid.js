"use strict";

//Inherits from GameObjectList

function GameObjectGrid(rows, columns, layer) {
    GameObjectList.call(this, layer);

    this.cellWidth = 0;
    this.cellHeight = 0;
    this._rows = rows;
    this._columns = columns;
}

GameObjectGrid.prototype = Object.create(GameObjectList.prototype);

//Override of GameObjectList add() 
//since sort is not needed and gameObject must be placed
//in grid position 
GameObjectGrid.prototype.add = function (gameobject) {
    var row = this._getGridRow(this._gameObjects.length);
    var col = this._getGridCol(this._gameObjects.length);
    this._gameObjects.push(gameobject);
    gameobject.parent = this;
	//GameObjects local pos
    gameobject.position = new Vector2(col * this.cellWidth, row * this.cellHeight);
};

// Vector2(x,y) --> x = col and y = row
GameObjectGrid.prototype.addAt = function (gameobject, col, row) {
    this._gameObjects[row * this._columns + col] = gameobject;
    gameobject.parent = this;
	//GameObjects local pos	
    gameobject.position = new Vector2(col * this.cellWidth, row * this.cellHeight);
};

GameObjectGrid.prototype.at = function (col, row) {
    var index = row * this._columns + col;
    return  (index < 0 || index >= this._gameObjects.length) ?
			null :
			this._gameObjects[index];
};

//An anchorPosition is the elements position within the Grid layout
GameObjectGrid.prototype.getAnchorPosition = function (gameobject) {
    var limit = this._gameObjects.length;
    for (var i = 0; i < limit; ++i)
        if (this._gameObjects[i] === gameobject) {
            var row = this._getGridRow(i);
            var col = this._getGridCol(i);
            return new Vector2(col * this.cellWidth, row * this.cellHeight); //You don't use the gameObject's position b/c it might not be on its anchorPosition
        }
    return Vector2.zero;
};

Object.defineProperty(GameObjectGrid.prototype, "rows", 
{
    get: function () { return this._rows;   }
});

Object.defineProperty(GameObjectGrid.prototype, "columns", 
{
    get: function () {   return this._columns;    }
});

/*******************
* Internal functions
********************/

GameObjectGrid.prototype._getGridRow = function(row){
	return Math.floor(row / this.columns);
}

GameObjectGrid.prototype._getGridCol = function(col){
	return col % this.columns;
}