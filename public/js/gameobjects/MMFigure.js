"use strict";

//Inherits from SpriteGameObject

function MMFigure(layer) {
    var random = Math.floor(Math.random() * 3) + 1; //Get random value between 1 and 3
    var spr = sprites["single_jewel" + random];
    SpriteGameObject.call(this, spr, layer);
}

MMFigure.prototype = Object.create(SpriteGameObject.prototype);

MMFigure.prototype.update = function (delta) {
    SpriteGameObject.prototype.update.call(this, delta);

    if (this.parent.dragging)
        return;
	
	//Parent should always be of type MMFigureGrid
	//Velocity is updated depending on how close the MMFigure is to its anchor position (position on the MMFigureGrid)
	//Thanks to velocity being calculated by the anchorPosition, the MMFigure will end up in its anchor position
	//since SpriteGameObject.update() adds this.position and this.velocity 
    var anchor = this.parent.getAnchorPosition(this);
    this.velocity = anchor.subtractFrom(this.position).multiplyWith(15);
};