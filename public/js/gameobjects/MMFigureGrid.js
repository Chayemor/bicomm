"use strict";

//Inherits from GameObjectGrid

function MMFigureGrid(rows, columns, layer) {
    GameObjectGrid.call(this, rows, columns, layer);
    this.dragging = false;
    this._dragRow = 0;
    this._draggingLastX = 0; //Last column dragged. Vector2(x,y) --> x = column, y = row
    this._touchIndex = 0;
}

MMFigureGrid.prototype = Object.create(GameObjectGrid.prototype);

MMFigureGrid.prototype.handleInput = function (delta) {
    /*if (Touch.isTouchDevice)
        this.handleInputTouch(delta);
    else
        this.handleInputMouse(delta);*/

	// reposition all figures in the row
	for (var i = 0; i < this.columns; i++) {
		var currObj = this.at(i, this._dragRow);
		currObj.position.x += Math.ceil(this.cellWidth/2);
	}
	
	// check if row shift is needed
	var firstObj = this.at(0, this._dragRow);
	if (firstObj.position.x < -this.cellWidth / 2 )
		this.shiftRowLeft(this._dragRow);
	else
		this.shiftRowRight(this._dragRow);
	
	
};

MMFigureGrid.prototype.setDragRow = function(newDragRow) {  
	this._dragRow = Math.abs(newDragRow % this._rows);
	console.log("PLAYING ROW = " + this._dragRow);
}
MMFigureGrid.prototype.getDragRow = function(){ return this._dragRow; }

MMFigureGrid.prototype.handleInputMouse = function (delta) {
    
	//Detect if drag has started and determine if said drag is
	//within the grid boundaries
	//local_position --> mouse.position - world.position
	if (Mouse.left.down && !this.dragging) {
		
        var rect = new Rectangle(this.worldPosition.x, this.worldPosition.y, this.columns * this.cellHeight, this.rows * this.cellWidth);
        if (Mouse.containsMouseDown(rect)) {
            this.dragging = true;
            this._dragRow = Math.floor((Mouse.position.y - this.worldPosition.y) / this.cellHeight);
            this._draggingLastX = Mouse.position.x - this.worldPosition.x;
        }
		
    }
	//¿has user stopped the dragging?
    if (!Mouse.left.down) 
        this.dragging = false;

    if (this.dragging) {
        var newpos = Mouse.position.x - this.worldPosition.x;

        // reposition all figures in the row
        for (var i = 0; i < this.columns; i++) {
            var currObj = this.at(i, this._dragRow);
            currObj.position.x += (newpos - this._draggingLastX);
        }
		
        // check if row shift is needed
        var firstObj = this.at(0, this._dragRow);
        if (firstObj.position.x < -this.cellWidth / 2 && newpos - this._draggingLastX < 0)
            this.shiftRowLeft(this._dragRow);
		
        var lastObj = this.at(this.columns - 1, this._dragRow);
        if (lastObj.position.x > (this.columns - 1) * this.cellWidth + this.cellWidth / 2 && newpos - this._draggingLastX > 0)
            this.shiftRowRight(this._dragRow);
		
        this._draggingLastX = newpos;
    }
};

MMFigureGrid.prototype.handleInputTouch = function (delta) {
    var pos, newpos;
    var rect = new Rectangle(this.worldPosition.x, this.worldPosition.y, this.columns * this.cellHeight, this.rows * this.cellWidth);

    if (Touch.isTouching && !this.dragging) {
        if (Touch.containsTouch(rect)) {
            this._touchIndex = Touch.getIndexInRect(rect);
            pos = Touch.getPosition(this._touchIndex);
            this.dragging = true;
            this._dragRow = Math.floor((pos.y - this.worldPosition.y) / this.cellHeight);
            this._draggingLastX = pos.x - this.worldPosition.x;
        }
    }
	
    if (!Touch.isTouching) 
        this.dragging = false;

    if (this.dragging) {
        pos = Touch.getPosition(this._touchIndex);
        newpos = pos.x - this.worldPosition.x;

        // reposition all figures in the row
        for (var i = 0; i < this.columns; i++) {
            var currObj = this.at(i, this._dragRow);
            currObj.position.x += (newpos - this._draggingLastX);
        }
		
        // check if row shift is needed
        var firstObj = this.at(0, this._dragRow);
        if (firstObj.position.x < -this.cellWidth / 2 && newpos - this._draggingLastX < 0)
            this.shiftRowLeft(this._dragRow);
		
        var lastObj = this.at(this.columns - 1, this._dragRow);
        if (lastObj.position.x > (this.columns - 1) * this.cellWidth + this.cellWidth / 2 && newpos - this._draggingLastX > 0)
            this.shiftRowRight(this._dragRow);
		
        this._draggingLastX = newpos;
    }
	
};

/* Row shifting takes first element (depends on dragging direction), copies to tmp variable. 
*  Shifts every element in row one column to left/right. To finish, funciton places temporarily stored
*  var in last place of row (depends on dragging direction);
*  -- Offset from first element is stored to later add it when its position is changed, giving way to a smooth dragging effect
*/

MMFigureGrid.prototype.shiftRowRight = function (selectedRow) {
    var lastObj = this.at(this._columns - 1, selectedRow);
    var positionOffset = lastObj.position.x - (this.columns - 1) * this.cellWidth;
    for (var x = this._columns - 1; x > 0; --x)
        this._gameObjects[selectedRow * this._columns + x] = this._gameObjects[selectedRow * this._columns + (x - 1)];
   
    this._gameObjects[selectedRow * this._columns] = lastObj;
    lastObj.position = new Vector2(-this.cellWidth + positionOffset, selectedRow * this.cellHeight);
};

MMFigureGrid.prototype.shiftRowLeft = function (selectedRow) {
    var firstObj = this.at(0, selectedRow);
    var positionOffset = firstObj.position.x;
    for (var x = 0; x < this._columns - 1; ++x)
        this._gameObjects[selectedRow * this._columns + x] = this._gameObjects[selectedRow * this._columns + x + 1];
    
	this._gameObjects[selectedRow * this._columns + (this._columns - 1)] = firstObj;
    firstObj.position = new Vector2(this._columns * this.cellWidth + positionOffset, selectedRow * this.cellHeight);
};
