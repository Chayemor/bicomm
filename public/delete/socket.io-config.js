var IO = 'undefined' ;

jQuery(function($){    
    'use strict';

    /**
     * Socket.IO code is collected in the IO namespace
     *
     */
    IO = {

        /**
         * Connect the Socket.IO client
         * to Socket.IO server
         */	 		 
        init: function() {
				if(!IO.room_id) {
					console.log("IO.room_id MUST BE SET before calling init. IO.room_id is your unique identifier.");
					return;
				}
				
                IO.connectionOptions =  {
                        'force new connection' : true,
                        'reconnection': true,
                        'reconnectionDelay': 2000,                  // starts with 2 secs delay, then 4, 6, 8, until 60 where it stays forever until it reconnects
                        'reconnectionDelayMax' : 30000,             // 1/2 minute maximum delay between connections
                        'reconnectionAttempts': "Infinity",         // prevent user from having to reconnect manually in order to prevent dead clients after a server restart
                        'timeout' : 10000,                          // before connect_error and connect_timeout are emitted
						'query'	  : { 'room_id' : IO.room_id}
                };
                IO.socket = io("http://bicomm.noip.me:8888", IO.connectionOptions);
                IO.bindEvents();
        },
		
		room_id :	false,

        bindEvents : function() {
                IO.socket.on('connect', IO.onConnected );
				IO.socket.on('receiveAction', IO.onReceiveAction );
                IO.socket.on('error', IO.error);
        },

        onConnected : function() {
			console.log("Connection successful");
        },
		
		onReceiveAction : function() {
			console.log("This has to be set.");
		},
   
        error : function(data) {
            console.log(data.message);
        }
    };

}($));