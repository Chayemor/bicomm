"use strict";
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'jkdhjdlfndk6865jgf';

module.exports = {

    'encrypt': function encrypt(content){
					var cipher = crypto.createCipher(algorithm, password);	
					var crypted = cipher.update(content, 'utf8', 'hex');
					crypted += cipher.final('hex');
					return crypted;
				} ,
	'decrypt' : function decrypt (content){
					var decipher = crypto.createDecipher(algorithm, password);
					var decrypted = decipher.update(content, 'hex', 'utf8');
					decrypted += decipher.final('utf8');
					return decrypted;
				},
	'validateEmail' : function validateEmail (email){
						//Regex expression from: http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
						 var regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
						//return regex.test(email);
						return true;
					},
	'validateString' : function validateString (text){
						//Regex expression from: http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
						 var regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
						//return regex.test(text);
						return true;
					},
	'secToken'	 		: 'security-token',
	'skipSignin'		: 'signed-in'

};