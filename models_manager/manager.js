"use strict";

var Model = "";
var resultFunc = function(err, result, callback){
					if(err) throw err;
					callback(result); 
				 };

var model_functions = {
	'checkModelVariable'  :	function(){
								var valid = Model != "";
								if(!valid) console.log("Model variable in manager has not been set");
								return valid;
							},
	
	'validateOptions'	  : function(options){
								["filterBy", "select", "update"].forEach(function(key){
									if( !(key in options) ) options[key] = {}; 
								});
								if( !('callback' in options) ) options.callback = function(){}; 	
							},	
	
	'setModel'    : function setModel(model){
								Model = model;
							},
	'clearModel'  : function clearModel(){
								Model = "";	
							},						

    'findOne'			   : function findOne(options){
								if( !model_functions.checkModelVariable() ) return;
								model_functions.validateOptions(options);
								Model.findOne( 
									options.filterBy ,
									options.select,	
									function(err, result){
										resultFunc(err, result, options.callback);
									}
								);
							},
							
	'find'					: function find(options){
								if( !model_functions.checkModelVariable() ) return;
								model_functions.validateOptions(options);
								Model.find( 
									options.filterBy ,
									options.select,	
									function(err, result){
										resultFunc(err, result, options.callback);
									}
								);
							},

	'update'				: function update(options){
								if( !model_functions.checkModelVariable() ) return;
								model_functions.validateOptions(options);
								console.log(options);
								Model.update( 
									options.filterBy ,
									options.update,	
									function(err, result){
										resultFunc(err, result, options.callback);
									}
								);
							}							
};

module.exports = model_functions;