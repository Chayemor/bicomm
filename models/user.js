var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model 
// pass it via module.exports
module.exports = mongoose.model('User', new Schema({ 
    name: String, 
	password: String,
	plan: String
}));