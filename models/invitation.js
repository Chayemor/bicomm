var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model 
// pass it via module.exports
module.exports = mongoose.model('Invitation', new Schema({ 
    number: String, 
	taken: Boolean,
	taken_by: String
}));