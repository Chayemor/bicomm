// ******************************************************************
// Packages
// ******************************************************************
var express= require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
//var morgan = require('morgan');
var mongoose = require('mongoose');

var token_mngr    = require('jsonwebtoken'); 
var security = require('./security');
var config = require('./config'); // Contains Mongoose connection data

// **********
// Models
// **********
var ModelManager = require('./models_manager/manager'); // Mongoose Model Manager
var User   = require('./models/user'); // Mongoose Model for user
var Invitation = require('./models/invitation'); // Mongoose Model for Invitation

//ENABLE CORS
app.all('/', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
 });
 
// ******************************************************************
// Set up
// ******************************************************************
var port = process.env.PORT || 8888;  
mongoose.connect(config.database);
app.set('token_sign_secret', config.jot_secret);
app.set('plans' , config.plans);

//Public file routes
app.use(express.static(__dirname + '/public'));
app.use('/images', express.static(__dirname + '/public/media/assets'));
app.use('/bicommtheme', express.static(__dirname + '/public/style'));

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//Morgan is used to log requests in the console
//app.use(morgan('dev'));

// ******************************************************************
// Route Set Up
// ******************************************************************

//User creation 
app.get('/signup', function(req, res){	
	res.sendFile(path.join(__dirname, 'public/signup.html'));
});
app.post('/signup', function(req, res){
	
	var email = req.body.email;
	var pass = req.body.password;
	var invitation_number = req.body.invitation_n;

	
	var valid_data = [security.validateEmail(email), security.validateString(pass), security.validateString(invitation_number) ];
	if(!valid_data[0] && !valid_data[1] && !valid_data[2]){
		res.json({success:false, message: "The data you have introduced is invalid"});
		return;
	}

	userExists({
				email:email, 
				password:pass, 
				callback : function(user_exists){
					
					if(user_exists){
						res.json({success:false, message: "Seems the user in question already exists."});
						return;
					}
					
					ModelManager.setModel(Invitation);
					ModelManager.findOne({
						filterBy : { number: invitation_number },
						callback : function(invitation){
							if(!invitation){
								res.json({success:false, message:'There seems to be no such invitation registered.'});
							} else {
								if(invitation.taken)
									res.json({success: false, message: 'We are sorry, but it seems someone has already used your invitation.' });
								else{
									signUserUp({ email:email, 
												 pass:pass, 
												 callback: function(result){

													if(result.success){ 
														disableInvitation({invitation_number:invitation_number, email:email});
														//JSON response must include security token for future requests
														createToken(email, pass, res);
													} else 													
														res.json(result);
												 } 
											  });
								}
							}
						} //END OF CALLBACK
					});
				} 
	});
	
});

var signUserUp = function(data){
	var user = new User({
		name: data.email,
		password: data.pass,
		plan: app.get('plans').Regular
	});
	
	user.save(function(err){
		if(err) throw err;

		data.callback({success: true});
	});	
};

var userExists = function(data){
	ModelManager.setModel(User);
	ModelManager.findOne({
		filterBy : { name : data.email },
		callback : function(user){
			data.callback( user != null );
		}
	});	
};

var disableInvitation = function(data){
	ModelManager.setModel(Invitation);
	ModelManager.update({
		filterBy : {number:data.invitation_number},
		update   : { $set: {taken:true, taken_by: data.email} },
		callback : function(result){
			//console.log("Invitation disabled");
		}
	});
};

//User signin 
app.get('/signin', function(req, res){

	//Check sign in cookie existence in header
	var cookies =  parseCookies(req.headers['cookie']);
	
	if(security.skipSignin in cookies && cookies[security.skipSignin])
		res.redirect('/auth_api/dashboard');
	else 
		res.sendFile(path.join(__dirname, 'public/signin.html'));
});
app.post('/signin', function(req, res){
	
	var email = req.body.email;
	var pass = req.body.password;

	
	var valid_data = [security.validateEmail(email), security.validateString(pass) ];
	if(!valid_data[0] && !valid_data[1]){
		res.json({success:false, message: "The data you have introduced is invalid"});
		return;
	}

	//JSON response must include security token for future requests
	createToken(email, pass, res);
});

var parseCookies = function(cookieHeader){
	var cookies = {};
	if(cookieHeader == '' || !cookieHeader)
		return cookies;
	
	cookieHeader.split(';').forEach( function(cookie){
		var values = cookie.split('=');
		cookies[values.shift().trim()] = values.join().trim();
	} );
	
	return cookies;
};

// ******************************************************************
// Authentication API
// ******************************************************************
var authApiRoutes = express.Router();

// Authenticate user (POST /auth_api/authenticate)
// Must be before middleware route in order to avoid securing it
authApiRoutes.post( '/authenticate', function(req, res){
	
	if( !security.validateString(req.body.name) || !security.validateString(req.body.password) )
	{
		res.json({success: false, message: 'Incorrect or invalid input for email, password or invitation'});
		return;
	}
	
	createToken(req.body.name, req.body.password, res);
	
});

var createToken = function(username, pass, res){
	ModelManager.setModel(User);
	ModelManager.findOne({
		filterBy : { name:username , password:pass },
		callback : function(user){
			if(!user){
				res.json({success:false, message:'Incorrect email or password.'});
			} else {
				//Encrypted token must be provided
				var token = token_mngr.sign( {'user' : user.name , 'plan' : user.plan }, app.get('token_sign_secret'), {expiresIn: "5000d"} ); 
				var expires_in = Date.now() + 432000000; // 432000000 ms = 5 days
				var security_cookie = security.secToken +'=' + security.encrypt(token) + ';expires='+expires_in+';path=/auth_api;domain=bicomm.noip.me;HttpOnly';
				var skip_sign_in_cookie = security.skipSignin+'=true;expires='+expires_in+';path=/signin;domain=bicomm.noip.me;HttpOnly';
				res.setHeader('Set-Cookie', [security_cookie, skip_sign_in_cookie] );
				res.json({success: true, message: 'success', token: token });
			}
		}
	});	
};

// Middleware makes sure user is authenticated
// MUST be placed before any routes that want to be secured
authApiRoutes.use(function(req,res,next){
	
	//console.log("Middleware getting checked");
	
	//Check security-token cookie existence in header
	var cookies =  parseCookies(req.headers['cookie']);
	
	if( ! security.secToken in cookies )
		return res.json({success: false, message: 'Failed to verify token.'});
	
	var token = cookies[security.secToken];
	//Verify token after decoding it
	if(token){
		
		var decoded_token = security.decrypt(token);
		
		token_mngr.verify(  decoded_token, 
							app.get('token_sign_secret'), 
							function(err, decoded){
								if(err){
									return res.json({success: false, message: 'Failed to verify token.'});
								} else {
									req.decoded = decoded;
									next();
								}
							}
		);
		
	} else {
		return res.status(403).send( {success: false, message: 'No token provided.' } );
	}
	
});

//(GET /auth_api/)
authApiRoutes.get('/', function(req, res) {	
  res.redirect('/auth_api/dashboard');
});

// return all users (GET /auth_api/users)
authApiRoutes.get('/users', function(req, res) {
	
	ModelManager.setModel(User);
	ModelManager.find({
		filterBy : {},
		select   : 'name plan',
		callback : function(users){
			res.json(users);
		}
	});
	
});

//Dashboard
// auth_api/dashboard
authApiRoutes.get('/dashboard', function(req, res){	
	res.sendFile(path.join(__dirname, 'public/dashboard.html'));
});

//File downloads
// auth_api/client
authApiRoutes.get('/client', function(req, res){	
	var file = path.join(__dirname, 'public/bicommFiles.tar.gz');
	res.download(file);
});

//Logout
// auth_api/logout
authApiRoutes.get('/logout', function(req, res){
	//Encrypted tokens must be removed
	var expires_in = -50; 
	var security_cookie = security.secToken +'=;expires='+expires_in+';path=/auth_api;domain=bicomm.noip.me;HttpOnly';
	var skip_sign_in_cookie = security.skipSignin+'=;expires='+expires_in+';path=/signin;domain=bicomm.noip.me;HttpOnly';
	res.setHeader('Set-Cookie', [security_cookie, skip_sign_in_cookie] );
	res.redirect('/signin');
});   

app.use('/auth_api', authApiRoutes);

// ******************************************************************
// Nothing has responded to request, assume it's a 404
// ******************************************************************
app.use(function(req, res, next){
  res.status(404);
  
  if (req.accepts('html')) {        // HTML response
    res.sendFile(path.join(__dirname, 'public/404.html'));
    return;
  } else if (req.accepts('json')) { // JSON Format response
    res.send({ error: 'Not found' });
    return;
  }

  res.type('txt').send('Not found'); // PLAINT TEXT
});


//HTTP Server 
var server = require('http').createServer(app).listen(8888);
//Socket.IO server
var io = require('socket.io').listen(server);



// ******************************************************************
// Socket.io
// ******************************************************************

//Join appropriate room on connection
io.use(function(socket, next){
    // return the result of next() to accept the connection.
    if (socket.handshake.query.room_id != "" || socket.handshake.query.room_id ) {
		socket.room_id = socket.handshake.query.room_id;
        return next();
    }
    // call next() with an Error to reject the connection.
    next(new Error('Authentication error'));
});

io.on('connection', function(socket){
	initConnection(socket);
	
	socket.on('controlAction', function(data){
		socket.broadcast.to(socket.room_id).emit('receiveAction', data);
	});
	
});

//Communication Framework 
function initConnection(socket){
	socket.join( socket.room_id ); 
}


